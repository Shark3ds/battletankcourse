// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Runtime/Engine/Classes/GameFramework/SpringArmComponent.h"
#include "TankAimingComponent.generated.h"


class UTankBarrel; //R|Forward Declaration
class UTankTurret; //R|Forward Declaration

//R|Holds Barresl Properties and Elevation Method
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	//R| Sets default values for this component's properties
	UTankAimingComponent();
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void AimAt(FVector HitLocation, float LaunchSpeed);

	void SetBarrelReference(UTankBarrel* BarrelToSet);
	void SetTurretReference(UTankTurret* TorretToSet);

	void MoveBarrelTowards(FVector, FVector);

	void MoveBarrelTowards_new(USceneComponent*);

private:
	//UStaticMeshComponent * Barrel = nullptr;

	UTankBarrel* Barrel = nullptr;   
	UTankTurret* Turret = nullptr;  

};
