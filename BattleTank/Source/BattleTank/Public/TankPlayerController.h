// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Tank.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

/**
 *
 */
UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
		
private:

	ATank * GetControllerTank() const;

	void BeginPlay() override;


	// Sets default values for this pawn's properties
	ATankPlayerController();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AimTowardsCrosshair();

	void GetSightRayHitLocation(FVector& OutHitLocation) const;

	UPROPERTY(EditAnyWhere)
		float CrossHairXLocation = 0.5;

	UPROPERTY(EditAnyWhere)
		float CrossHairYLocation = 0.3333333;
	 
	USceneComponent* GetBPComponent(FString ComponentName);

	void TurnTurretWithYawAxis();
	void TurnBarrelWithPitchAxix();
};
