// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankBarrel.generated.h"


//R|Tank Barrel
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent)) 
class BATTLETANK_API UTankBarrel : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	// -1 is max downlard speed and +1 is max up movement
	void Elevate(float RelativeSpeed);

private:

	UPROPERTY(EditAnyWhere, Category = Setup)
		float MaxDegreePerSecond = 5;
	UPROPERTY(EditAnyWhere, Category = Setup)
		float MaxElevationDegree = 40; 
	UPROPERTY(EditAnyWhere, Category = Setup)
		float MinElevationDegree = 0;
};
