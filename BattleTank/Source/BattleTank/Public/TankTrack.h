// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"

 //R|Tank Track
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()

public:

	// Sets default values for this pawn's properties
	UTankTrack();

 
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	 

public:
	//Set Movement
	UFUNCTION(BlueprintCallable, Category = Input)
		void SetThrottle(float Throttle);


	//Max Force per Track  (* 10000)
	UPROPERTY(EditAnywhere, Category = Input)
		float TrackMaxDrivingForce = 4000;

	void GetSightRayHitLocation(FVector& OutHitLocation) const;

	void SetTrackAlwaysInContact();

};
