// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Tank.h"
#include "CoreMinimal.h"
#include "AIController.h"
#include "GameFramework/PlayerController.h"
#include "TankAiController.generated.h"

/**
 *
 */
UCLASS()
class BATTLETANK_API ATankAiController : public AAIController
{
	GENERATED_BODY()

public:
	 ATankAiController();

private:

	virtual	void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

};
