// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "TankAimingComponent.h"
#include "Tank.generated.h"

class UTankBarrel;
class UTankTurret;
class UTankAimingComponent;
class AProjectile;

UCLASS()
class BATTLETANK_API ATank : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATank();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UTankAimingComponent* TankAimingComponent = nullptr;


public:

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UTankAimingComponent* GetAimingComponent();


	UFUNCTION(BlueprintCallable)
		void Fire();

	void AimAt(FVector Location);


	UFUNCTION(BlueprintCallable, Category = Setup)
		void SetBarrelReference(UTankBarrel* BarrelToSet);

	UFUNCTION(BlueprintCallable, Category = Setup)
		void SetTurretReference(UTankTurret* BarrelToSet);

	UPROPERTY(EditAnyWhere, Category = Firing)
		float LaunchSpeed = 4000;

	UPROPERTY(EditAnyWhere, Category = Setup) 
		float ReloadtimeinSeconds = 3;

	UPROPERTY(EditAnywhere, Category = Setup)
		//UClass* ProjectileBlueprint; //R: U can do that, but it's very generic and you can put anything in it.
		TSubclassOf<AProjectile> ProjectileBlueprint; //R: This works a lot way better (You need to put .h in CPP

	//Local Reference to Barrel (Just to get it when SetBarrelReference is used

	UTankBarrel* Barrel = nullptr;

private:

	float LastFireTime = 0;
};
