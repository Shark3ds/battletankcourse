// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTrack.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"


// Sets default values
UTankTrack::UTankTrack()
{
 

}

// Called every frame
void UTankTrack::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UE_LOG(LogTemp, Warning, TEXT("Called!!!!"))
}

void UTankTrack::SetThrottle(float Throttle) {

	auto ForceApplied = GetForwardVector() * Throttle * TrackMaxDrivingForce * 1000;

	auto ForceLocation = GetComponentLocation();

	auto TankRoot = Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent());

	TankRoot->AddForceAtLocation(ForceApplied, ForceLocation);

	auto ForcePoint = ForceLocation + ForceApplied;
	DrawDebugLine(GetWorld(), ForceLocation, ForcePoint, FColor(255, 0, 0), false, 1, 0, 10);

	//DrawDebugPoint(GetWorld(), ForcePoint, 10, FColor(255, 0, 0), true, 2, 10);

	SetTrackAlwaysInContact();

}

void UTankTrack::SetTrackAlwaysInContact() {

	//Always force The Floor Contact with the Tracks
	FVector OutHitLocation;
	GetSightRayHitLocation(OutHitLocation);

	//If the diference of component and floor is above some value, add force to make them together
	float Diff = (abs(OutHitLocation.Z - GetComponentLocation().Z));
	if (Diff > 12) {
		UE_LOG(LogTemp, Warning, TEXT("Using Force. Diference is %f"), Diff)

			auto TankRoot = Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent());

		TankRoot->AddForceAtLocation(FVector(GetComponentLocation().X, GetComponentLocation().Y,
			-10000000000), GetComponentLocation());
	}
}

void UTankTrack::GetSightRayHitLocation(FVector & OutHitLocation) const
{

	FHitResult Hit;
	FVector Start = GetComponentLocation();
	FVector End = { GetComponentLocation().X,GetComponentLocation().Y,-1000 };

	bool HitSomething = GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		Start,
		End,
		FCollisionObjectQueryParams(FCollisionObjectQueryParams::DefaultObjectQueryParam),
		FCollisionQueryParams("", false, GetOwner())
	);

	if (HitSomething) {
		//	UE_LOG(LogTemp, Warning, TEXT("Hit ActorName: %s"), *Hit.GetActor()->GetName())
		OutHitLocation = Hit.Location;
	}
	else {

		//UE_LOG(LogTemp, Warning, TEXT("Hit Nothing"))
	}

	DrawDebugLine(GetWorld(),
		Start,
		End,
		FColor(0, 255, 0), false, -1.f, 0, 2);

}