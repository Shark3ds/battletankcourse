// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTurret.h"
#include "Engine/Engine.h"

#define M_PRINT(x, Color) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 3.0f, Color, x);}



void UTankTurret::Rotate(float RelativeSpeed) {

	if (RelativeSpeed > 180) {
		RelativeSpeed = RelativeSpeed * -1;
	}
	else if (RelativeSpeed < -180) {
		RelativeSpeed = RelativeSpeed * -1;
	}

	RelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -1, 1);
	auto RotationChange = RelativeSpeed * MaxDegreePerSecond * GetWorld()->DeltaTimeSeconds;

	auto Delta = abs(RelativeRotation.Yaw + RotationChange);

	auto RawNewRotation = RelativeRotation.Yaw + RotationChange;

	SetRelativeRotation(FRotator(0, RawNewRotation, 0));

	
}

