// Fill out your copyright notice in the Description page of Project Settings.


#include "Tank.h"
#include "Engine/Engine.h"
#include "TankBarrel.h"
#include "Projectile.h" 

#define M_PRINT(x, Color) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 3.0f, Color, x);}


// Sets default values
ATank::ATank()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//R|This create the Aiming Component in BluePrint of Tank!
	TankAimingComponent = CreateDefaultSubobject<UTankAimingComponent>(FName("R|Aiming Component"));


}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();

}



// Called to bind functionality to input
void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UTankAimingComponent * ATank::GetAimingComponent()
{
	return TankAimingComponent;
}

void ATank::Fire()
{

	if ((abs(FPlatformTime::Seconds() - LastFireTime) > ReloadtimeinSeconds)) {

		if (!Barrel) { return; }

		if (!ProjectileBlueprint) {
			M_PRINT("ProjectileBlueprint is Empty", FColor(234, 221, 111))
		}

		//Spawn Projectile
		AProjectile* Projectile =
			GetWorld()->SpawnActor<AProjectile>(
				ProjectileBlueprint,
				Barrel->GetSocketLocation("Projectile"),
				Barrel->GetSocketRotation("Projectile")
				);


		if (Projectile) {
			Projectile->LaunchProjectile(LaunchSpeed);
			LastFireTime = FPlatformTime::Seconds();
		}
		else {
			//	M_PRINT("Projectile Fail...", FColor(234, 221, 111))
		}

	}

}

void ATank::AimAt(FVector Location)
{

	TankAimingComponent->AimAt(Location, LaunchSpeed);

}

void ATank::SetBarrelReference(UTankBarrel * BarrelToSet)
{
	TankAimingComponent->SetBarrelReference(BarrelToSet);
	//Get for local reference

	Barrel = BarrelToSet;
}

void ATank::SetTurretReference(UTankTurret * TurretToSet)
{
	TankAimingComponent->SetTurretReference(TurretToSet);


}

