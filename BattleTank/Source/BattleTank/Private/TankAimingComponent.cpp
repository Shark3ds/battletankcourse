// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAimingComponent.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/Engine.h"

#define M_PRINT(x, Color) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 3.0f, Color, x);}



// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTankAimingComponent::AimAt(FVector HitLocation, float LaunchSpeed) {

	if (!Barrel) { return; }

	FVector OutLauncVelocity;
	FVector StartLocation = Barrel->GetSocketLocation(FName("Projectile"));
	bool HasSolution;

	if (HitLocation.X == 0 && HitLocation.Y == 0 && HitLocation.Z == 0) {
		HasSolution = false;
	}
	else {
		HasSolution = UGameplayStatics::SuggestProjectileVelocity(
			this,
			OutLauncVelocity,
			StartLocation,
			HitLocation,
			LaunchSpeed,
			false,
			0,
			0,
			ESuggestProjVelocityTraceOption::DoNotTrace //R|THis need to be here to prevent bug
		);

	}

	if (!HasSolution) { return; }
	/*

		OutLauncVelocity.X = OutLauncVelocity.X - 1;*/

	auto AimDirection = OutLauncVelocity.GetSafeNormal();
	auto TankName = GetOwner()->GetName();


	MoveBarrelTowards(AimDirection, HitLocation);

	//UE_LOG(LogTemp, Warning, TEXT("Yaw Camera %f"),  AimDirection.Z);

	/*UE_LOG(LogTemp,
		Warning,
		TEXT("Time: %f --> Hit Location at %s. AimDiretion would be %s "),
		time,
		*HitLocation.ToString(),
		*AimDirection.ToString());*/

		/*}
		else {*/
		//UE_LOG(LogTemp, Warning, TEXT("Has No solution..."), *TankName, *AimDirection.ToString());
	//}
}

void UTankAimingComponent::SetBarrelReference(UTankBarrel* BarrelToSet) {
	if (!BarrelToSet) { return; }
	Barrel = BarrelToSet;
}

void UTankAimingComponent::SetTurretReference(UTankTurret* TurretToSet) {
	if (!TurretToSet) { return; }
	Turret = TurretToSet;
}
void UTankAimingComponent::MoveBarrelTowards(FVector AimDirection, FVector HitLocation) {

	FRotator BarrelRotation = Barrel->GetForwardVector().Rotation();
	FRotator AimRotation = AimDirection.Rotation();
	FRotator DeltaRotator = AimRotation - BarrelRotation;

	/*M_PRINT(BarrelRotation.ToString(), FColor(255, 0, 0))
		M_PRINT(AimRotation.ToString(), FColor(0, 255, 0))*/
	//M_PRINT(FString::SanitizeFloat(DeltaRotator.Yaw), FColor(0, 0, 255))

		Barrel->Elevate(DeltaRotator.Pitch);
	Turret->Rotate(DeltaRotator.Yaw);


	/*if (HitLocation.X == 0 && HitLocation.Y == 0 && HitLocation.Z == 0) {

	}else{

		FRotator YawRot = Turret->GetForwardVector().Rotation();
		FRotator YawAim = HitLocation.Rotation();
		FRotator DeltaYaw = YawAim - YawRot;

		Turret->Rotate(DeltaYaw.Yaw);

	}*/
}

void UTankAimingComponent::MoveBarrelTowards_new(USceneComponent* SpringArm) {

	FRotator BarrelRotation = Barrel->GetForwardVector().Rotation();
	FRotator AimRotation = SpringArm->GetComponentRotation();
	FRotator DeltaRotator = AimRotation - BarrelRotation;

	DeltaRotator.Pitch = DeltaRotator.Pitch + 28; 

	Barrel->Elevate(DeltaRotator.Pitch);

}