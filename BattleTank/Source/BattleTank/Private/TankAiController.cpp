// Fill out your copyright notice in the Description page of Project Settings.


#include "TankAiController.h"
#include "BattleTank.h"
#include "Engine/Engine.h"

#define M_PRINT(x, Color) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 3.0f, Color, x);}



// Sets default values
ATankAiController::ATankAiController()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called every frame
void ATankAiController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	auto PlayerTank = Cast<ATank>(GetWorld()->GetFirstPlayerController()->GetPawn());
	auto ControlledTank = Cast<ATank>(GetPawn());

	if (PlayerTank) {

		ControlledTank->AimAt(PlayerTank->GetActorLocation());

		ControlledTank->Fire();
	}


}
 

void ATankAiController::BeginPlay()
{
	Super::BeginPlay();

}

 