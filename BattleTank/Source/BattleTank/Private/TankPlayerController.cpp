// Fill out your copyright notice in the Description page of Project Settings.


#include "TankPlayerController.h"
#include "BattleTank.h"
#include "Engine/Engine.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"



#define M_PRINT(x, Color) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 1.0f, Color, x);}


// Sets default values
ATankPlayerController::ATankPlayerController()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called every frame
void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AimTowardsCrosshair();
	//TurnTurretWithYawAxis(); //Deactivate, we will use the same than Above
}


ATank* ATankPlayerController::GetControllerTank() const {

	return Cast<ATank>(GetPawn());
}

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();

	auto ControlledTank = GetControllerTank();
	if (!ControlledTank) {
		M_PRINT("I'm without Possess", FColor::Red)

			///Try to set An event for Unpossess Event
	}
	else {
		M_PRINT(GetPawn()->GetName(), FColor::Red)
	}
}



void ATankPlayerController::GetSightRayHitLocation(FVector & OutHitLocation) const
{
	int32 ViewPortSizeX, ViewPortSizeY;

	//Get ViewPort Proportion in a 2D Vector
	GetViewportSize(OUT ViewPortSizeX, OUT ViewPortSizeY);

	//This doesnt work...i dont know why...
	FVector2D ScreenLocation = FVector2D(ViewPortSizeX * CrossHairXLocation, ViewPortSizeY * CrossHairYLocation);

	//Get Screen Direction from 2D ViewPort
	FVector  CameraWorldLocation;
	FVector  CameraWorldDirection;

	DeprojectScreenPositionToWorld
	(
		ViewPortSizeX * CrossHairXLocation,
		ViewPortSizeY * CrossHairYLocation,
		CameraWorldLocation,
		CameraWorldDirection
	);

	FHitResult Hit;
	FVector Start = CameraWorldLocation;
	FVector End = CameraWorldDirection * 100000000.f;

	bool HitSomething = GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		Start,
		End,
		FCollisionObjectQueryParams(FCollisionObjectQueryParams::DefaultObjectQueryParam),
		FCollisionQueryParams("", false, GetControllerTank())
	);

	if (HitSomething) {
		//UE_LOG(LogTemp, Warning, TEXT("Hit ActorName: %s"), *Hit.GetActor()->GetName())
		OutHitLocation = Hit.Location;

	}

	/*DrawDebugLine(GetWorld(),
		Start,
		End,
		FColor(0, 255, 0), false, -1.f, 0, 2);*/
	
}



void ATankPlayerController::AimTowardsCrosshair() {

	if (!GetControllerTank()) { return; }
	
	//R|Lets Mote Barrel According with Aiming Direction. It's to Easy automatic Aiming...
	FVector HitLocation = {0,0,0};
	GetSightRayHitLocation(HitLocation);
	GetControllerTank()->AimAt(HitLocation);
}




USceneComponent* ATankPlayerController::GetBPComponent(FString ComponentName) {

	//This is only to get FirePoint in Tank Blueprint and other Components
	ATank* Tank = GetControllerTank();

	TArray<USceneComponent*> Components; //Cria um Array de ponteiros 

	Tank->GetComponents<USceneComponent>(Components); //Pega os Componentes fazendo um Cast e coloca no Array

	for (int32 i = 0; i < Components.Num(); i++) //Count is zero
	{
		USceneComponent* Scenecomp = Components[i]; //null

		if (Scenecomp->GetName() == ComponentName) {
			return Scenecomp;
		}
	}

	return nullptr;
}

void ATankPlayerController::TurnTurretWithYawAxis()
{

	auto Gimbal = GetBPComponent("Gimbal");
	auto Turret = GetBPComponent("Turret");

	if (Turret && Gimbal) {
		Turret->SetWorldRotation(FRotator(0, Gimbal->GetComponentRotation().Yaw, 0));
	}

}

void ATankPlayerController::TurnBarrelWithPitchAxix()
{
}
